#ifndef __TEST_H__
#define __TEST_H__

#include "abteilung.h"
#include "angestellter.h"

extern const dept_t DEPARTMENTS[];
extern const emp_t EMPLOYEES[];

void Test();

#endif
