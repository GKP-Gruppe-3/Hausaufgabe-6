#ifndef __list_H__
#define __list_H__

#include "util.h"

typedef struct list_node list_node_t;
struct list_node
{
  list_node_t *prev;
  list_node_t *next;
  void *data;
};

typedef struct list list_t;
struct list
{
  list_node_t *first;
  list_node_t *last;
  size_t count;
  action_fn on_node_free;
};

void list_Ctor(list_t *list, action_fn on_node_free);

void list_Prepend(list_t *list, const void *const data);

void list_Append(list_t *list, const void *const data);

void list_InsertBefore(list_t *list, list_node_t *node, const void *const data);

void list_InsertAfter(list_t *list, list_node_t *node, const void *const data);

list_node_t *list_GetFirstNode(const list_t *const list);

list_node_t *list_GetLastNode(const list_t *const list);

list_node_t *list_GetNodeByIndex(const list_t *const list, size_t index);

list_node_t *list_node_GetPrev(const list_node_t *const node);

list_node_t *list_node_GetNext(const list_node_t *const node);

void list_node_GetData(const list_node_t *const node, void **data);

list_node_t *list_GetNodeByData(const list_t *const list, void *data);

void list_ForEach(const list_t *const list, action_fn action);

void list_Sort(list_t *list, compare_fn compare, bool asc);

void list_Remove(list_t *list, list_node_t *node);

void list_Truncate(list_t *list);

void list_Dtor(list_t *list);

#endif
