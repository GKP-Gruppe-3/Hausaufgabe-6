#ifndef __ANGESTELLTER_H__
#define __ANGESTELLTER_H__

#include "util.h"
#include "datum.h"

typedef struct dept dept_t;

/***************************************
* Aufgabe 1.2 - Verbund f�r Angestellte:
***************************************/

typedef enum job_type job_type_e;
enum job_type
{
  JT_MANAGER,     // Leiter
  JT_EMPLOYEE,    // Mitarbeiter
  JT_APPRENTICE,  // Azubi
  JT_TRAINEE      // Praktikant
};

typedef struct emp emp_t;
struct emp
{
  char *first_name;           // Vorname (Zeichenkette)
  char *last_name;            // Nachname (Zeichenkette)
  int32_t personnel_no;       // Personalnummer (Ganzzahl)
  job_type_e job_type;        // Position (Aufz�hlungstyp, Werte s.u.)
  dept_t *dept;               // Abteilung (Pointer auf Abteilung->s.Aufgabe i)
  date_t birth_date;          // Geburtsdatum (Typ Datum->letzte Hausaufgabe)
  date_t hire_date;           // Einstellungsdatum (Typ Datum -> letzte Hausaufgabe)
  double sal;                 // Gehalt (Gleitkommazahl)
  bool free_strings_in_dtor;  // M�ssen die Zeichenketten beim Zerst�ren freigegeben werden?
};

/**************************************/

void emp_Ctor(
  emp_t *emp,
  char *first_name,
  char *last_name,
  int32_t personnel_no,
  job_type_e job_type,
  dept_t *dept,
  date_t birth_date,
  date_t hire_date,
  double sal,
  bool free_strings_in_dtor);

emp_t *emp_Alloc(
  char *first_name,
  char *last_name,
  int32_t personnel_no,
  job_type_e job_type,
  dept_t *dept,
  date_t birth_date,
  date_t hire_date,
  double sal,
  bool free_strings_in_dtor);

void emp_Dtor(emp_t *emp);

void emp_Free(emp_t *emp);

dept_t *emp_GetDept(emp_t *emp);

void emp_SetDept(emp_t *emp, dept_t *dept);

/***************************************
* Aufgabe 1.4 - Funktionen f�r Angestellter (Implementation):
***************************************/

// Erstellen eines neuen Angestellten.
// Die Funktion soll dabei interaktiv die Werte f�r
// Vorname und Nachname, Personalnummer, Position,
// Geburts- und Einstellungsdatum sowie Gehalt abfragen.
emp_t emp_GetFromConsole();

// Das Alter des Angestellten in Jahren zur�ckgeben.
uint8_t emp_GetAge(const emp_t *const emp);

// Die Betriebszugeh�rigkeit des Angestellten in Jahren zur�ckgeben.
uint8_t emp_GetSeniority(const emp_t *const emp);

void emp_PutToConsole(const emp_t *const emp, const char *const message, ...);

#endif
