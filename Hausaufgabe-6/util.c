﻿#include "stdafx.h"

#include "util.h"

// Die gesamte Ausgabe bereinigen.
void ClearScreen()
{
  system("cls");
}

// Die Ein-/Ausgabe auf der Konsole pausieren,
// und anschließend die gesamte Ausgabe bereinigen.
void Pause()
{
  system("pause");
  system("cls");
}

#define INPUT_SIZE 64

// Eine Zeichenkette von der Konsole lesen.
void string_GetFromConsole(char *str, int max_chars, const char *const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }

  gets_s(str, max_chars);
  size_t i = strlen(str) - 1;
  if (str[i] == '\r' || str[i] == '\n')
  {
    str[i] = '\0';
  }
}

// Einen "int"-Wert von der Konsole lesen.
int int_GetFromConsole(const char *const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  char str[INPUT_SIZE];
  gets_s(str, INPUT_SIZE);
  return atoi(str);
}

// Einen "long"-Wert von der Konsole lesen.
long long_GetFromConsole(const char *const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  char str[INPUT_SIZE];
  gets_s(str, INPUT_SIZE);
  return atol(str);
}

// Einen "double"-Wert von der Konsole lesen.
double double_GetFromConsole(const char *const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  char str[INPUT_SIZE];
  gets_s(str, INPUT_SIZE);
  return atof(str);
}

char GetOption(char *options, const char *const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  char ch[30];
  //int ch;
  for (;;)
  {
    gets_s(ch, 30);
    _strlwr_s(ch, 30);
    //ch = tolower(_getch());
    for (char *opt = options; *opt != '\0'; opt++)
    {
      if (*ch == *opt)
      //if ((char)ch == *opt)
      {
        return *ch;
        //return ch;
      }
    }
  }
  return '\0';
  //return -1;
}
