#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#define REALLOC(type, size) (type*) realloc((size_t)(size))
#define MALLOC_SIZE(size) malloc((size_t)(size))
#define MALLOC_ARRAY(type, size) (type*) MALLOC_SIZE((size) * sizeof(type))
#define MALLOC(type) MALLOC_ARRAY(type, 1)
#define FREE(var) free(var); var = NULL

typedef int_fast8_t cmp_t;

#define CMP_LESS -1
#define CMP_EQUAL 0
#define CMP_GREATER 1

typedef void (*action_fn)(void *data);

typedef cmp_t (*compare_fn)(void *left, void *right);

void ClearScreen();

// Die Ein-/Ausgabe auf der Konsole pausieren,
// und anschließend die gesamte Ausgabe bereinigen.
void Pause();

// Eine Zeichenkette von der Konsole lesen.
void string_GetFromConsole(char *str, int max_chars, const char *const message, ...);

// Einen "int"-Wert von der Konsole lesen.
int int_GetFromConsole(const char *const message, ...);

// Einen "long"-Wert von der Konsole lesen.
long long_GetFromConsole(const char *const message, ...);

// Einen "double"-Wert von der Konsole lesen.
double double_GetFromConsole(const char *const message, ...);

char GetOption(char *options, const char *const message, ...);

#endif
