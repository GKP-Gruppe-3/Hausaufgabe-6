#include "stdafx.h"

#include "angestellter.h"
#include "abteilung.h"
#include "datum.h"

char *job_type_ToString(job_type_e job_type)
{
  char *str;
  switch (job_type)
  {
    case JT_MANAGER:
      str = "Leiter";
      break;
    case JT_EMPLOYEE:
      str = "Mitarbeiter";
      break;
    case JT_APPRENTICE:
      str = "Azubi";
      break;
    case JT_TRAINEE:
      str = "Praktikant";
      break;
    default:
      str = "";
      break;
  }
  return str;
}

void emp_Ctor(
  emp_t *emp,
  char *first_name,
  char *last_name,
  int32_t personnel_no,
  job_type_e job_type,
  dept_t *dept,
  date_t birth_date,
  date_t hire_date,
  double sal,
  bool free_strings_in_dtor)
{
  emp->first_name = first_name;
  emp->last_name = last_name;
  emp->personnel_no = personnel_no;
  emp->job_type = job_type;
  emp->dept = dept;
  emp->birth_date = birth_date;
  emp->hire_date = hire_date;
  emp->sal = sal;
  emp->free_strings_in_dtor = free_strings_in_dtor;

  if (dept != NULL)
  {
    dept_AddEmp(dept, emp);
  }
}

emp_t *emp_Alloc(
  char *first_name,
  char *last_name,
  int32_t personnel_no,
  job_type_e job_type,
  dept_t *dept,
  date_t birth_date,
  date_t hire_date,
  double sal,
  bool free_strings_in_dtor)
{
  emp_t *emp = MALLOC(emp_t);
  emp_Ctor(emp, first_name, last_name, personnel_no, job_type, dept, birth_date, hire_date, sal, free_strings_in_dtor);
  return emp;
}

void emp_Dtor(emp_t *emp)
{
  if (emp->dept != NULL)
  {
    dept_RemEmp(emp->dept, emp);
  }
  if (emp->free_strings_in_dtor)
  {
    free(emp->first_name);
    free(emp->last_name);
  }
}

void emp_Free(emp_t *emp)
{
  emp_Dtor(emp);
  free(emp);
}

dept_t *emp_GetDept(emp_t *emp)
{
  return emp->dept;
}

void emp_SetDept(emp_t *emp, dept_t *dept)
{
  if (emp->dept != dept)
  {
    dept_t *old_dept = emp->dept;
    emp->dept = dept;
    if (old_dept != NULL)
    {
      dept_RemEmp(old_dept, emp);
    }
    if (dept != NULL)
    {
      dept_AddEmp(dept, emp);
    }
  }
}

/***************************************
* Aufgabe 1.4 - Funktionen f�r Angestellter (Implementation):
***************************************/

// Erstellen eines neuen Angestellten.
// Die Funktion soll dabei interaktiv die Werte f�r
// Vorname und Nachname, Personalnummer, Position,
// Geburts- und Einstellungsdatum sowie Gehalt abfragen.
emp_t emp_GetFromConsole()
{
  const max_chars = 30;
  emp_t emp;
  emp.first_name = MALLOC_ARRAY(char, max_chars);
  emp.last_name = MALLOC_ARRAY(char, max_chars);
  printf("Bitte geben sie die Daten des Angestellten ein...\n");
  string_GetFromConsole(emp.first_name, max_chars, "  Vorname: ");
  string_GetFromConsole(emp.last_name, max_chars, "  Nachname: ");
  emp.personnel_no = int_GetFromConsole("  Personalnummer: ");
  emp.job_type = (job_type_e)int_GetFromConsole("  Position (0 => Manager; 1 => Mitarbeiter; 2 => Azubi; 3 => Praktikant): ");
  emp.birth_date = date_GetFromConsole("  Geburtsdatum (DD.MM.YYYY): ");
  emp.hire_date = date_GetFromConsole("  Einstellungsdatum (DD.MM.YYYY): ");
  emp.sal = double_GetFromConsole("  Gehalt: ");
  emp.dept = NULL;
  emp.free_strings_in_dtor = true;
  return emp;
}

// Das Alter des Angestellten in Jahren zur�ckgeben. 
uint8_t emp_GetAge(const emp_t *const emp)
{
  date_t now = date_Now();
  return date_DiffYears(&now, &emp->birth_date);
}

// Die Betriebszugeh�rigkeit des Angestellten in Jahren zur�ckgeben.
uint8_t emp_GetSeniority(const emp_t *const emp)
{
  date_t now = date_Now();
  return date_DiffYears(&now, &emp->hire_date);
}

void emp_PutToConsole(const emp_t *const emp, const char *const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  printf(
    "%s %s (P#%010d %s %s), geb.: %04d-%02d-%02d, eingest.: %04d-%02d-%02d, Gehalt: %05.02lf Euro",
    emp->first_name,
    emp->last_name,
    emp->personnel_no,
    job_type_ToString(emp->job_type),
    emp->dept->name,
    emp->birth_date.year,
    emp->birth_date.month,
    emp->birth_date.day,
    emp->hire_date.year,
    emp->hire_date.month,
    emp->hire_date.day,
    emp->sal);
}
