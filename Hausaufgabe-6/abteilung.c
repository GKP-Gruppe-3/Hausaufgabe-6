#include "stdafx.h"

#include "abteilung.h"
#include "angestellter.h"

void dept_Ctor(dept_t *dept, char *name, char *address, int32_t dept_no, bool free_strings_in_dtor)
{
  dept->name = name;
  dept->address = address;
  dept->dept_no = dept_no;
  list_Ctor(&dept->emps, NULL);
  dept->free_strings_in_dtor = free_strings_in_dtor;
}

dept_t *dept_Alloc(char *name, char *address, int32_t dept_no, bool free_strings_in_dtor)
{
  dept_t *dept = MALLOC(dept_t);
  dept_Ctor(dept, name, address, dept_no, free_strings_in_dtor);
  return dept;
}

void dept_Dtor(dept_t *dept)
{
  list_Dtor(&dept->emps);
  if (dept->free_strings_in_dtor)
  {
    free(dept->name);
    free(dept->address);
  }
}

void dept_Free(dept_t *dept)
{
  dept_Dtor(dept);
  free(dept);
}

cmp_t CompareEmpAge(emp_t *left, emp_t *right)
{
  return date_Compare(&left->birth_date, &right->birth_date);
}

cmp_t CompareEmpSeniority(emp_t *left, emp_t *right)
{
  return date_Compare(&left->hire_date, &right->hire_date);
}

/***************************************
* Aufgabe 1.3 - Funktionen f�r Abteilung (Implementation):
***************************************/

// Erstellen einer neuen Abteilung erm�glicht.
// Die Funktion soll interaktiv vom Nutzer Werte f�r Name,
// Adresse und Abteilungsnummer abfragen und eine entsprechende Abteilung erstellen.
dept_t dept_GetFromConsole()
{
  const max_chars = 30;
  dept_t dept;
  dept.name = MALLOC_ARRAY(char, max_chars);
  dept.address = MALLOC_ARRAY(char, max_chars);
  printf("Bitte geben sie die Daten der Abteilung ein...\n");
  string_GetFromConsole(dept.name, max_chars, "  Name: ");
  string_GetFromConsole(dept.address, max_chars, "  Adresse: ");
  dept.dept_no = int_GetFromConsole("  Abteilungsnummer: ");
  list_Ctor(&dept.emps, NULL);
  dept.free_strings_in_dtor = true;
  return dept;
}

// Hinzuf�gen eines neuen Angestellten zur Abteilung.
void dept_AddEmp(dept_t *dept, emp_t *emp)
{
  list_node_t *node = list_GetNodeByData(&dept->emps, emp);
  if (node == NULL)
  {
    list_Append(&dept->emps, emp);
  }
  if (emp_GetDept(emp) != dept)
  {
    emp_SetDept(emp, dept);
  }
}

// Entfernen eines Angestellten aus einer Abteilung.
void dept_RemEmp(dept_t *dept, emp_t *emp)
{
  list_node_t *node = list_GetNodeByData(&dept->emps, emp);
  if (node != NULL)
  {
    list_Remove(&dept->emps, node);
  }
  if (emp_GetDept(emp) == dept)
  {
    emp_SetDept(emp, NULL);
  }
}

// Einen Angestellten von einer Abteilung A zu einer Abteilung B verschieben.
void dept_TransEmp(dept_t *trg, dept_t *src, emp_t *emp)
{
  dept_RemEmp(src, emp);
  dept_AddEmp(trg, emp);
}

// Berechnung des Gesamtgehaltes der Abteilung
// (also die Summe aller Geh�lter der Angestellten einer Abteilung).
double dept_GetSalSum(const dept_t *const dept)
{
  emp_t *emp;
  double sal_sum = 0;
  for (
    list_node_t *node = list_GetFirstNode(&dept->emps);
    node != NULL;
    node = list_node_GetNext(node))
  {
    list_node_GetData(node, &emp);
    if (emp != NULL)
    {
      sal_sum += emp->sal;
    }
  }
  return sal_sum;
}

// Den (kalendarisch) �ltesten Angestellten der Abteilung zur�ck gibt.
const emp_t *const dept_GetEmpByMaxAge(const dept_t *const dept)
{
  emp_t *emp = NULL;
  emp_t *current;
  list_node_t *node = list_GetFirstNode(&dept->emps);
  list_node_GetData(node, &emp);
  for (
    node = list_node_GetNext(node);
    node != NULL;
    node = list_node_GetNext(node))
  {
    list_node_GetData(node, &current);
    if (CompareEmpAge(emp, current) == CMP_GREATER)
    {
      emp = current;
    }
  }
  return emp;
}

// Den Angestellten mit der l�ngsten Betriebszugeh�rigkeit zur�ckgeben.
const emp_t *const dept_GetEmpByMaxSeniority(const dept_t *const dept)
{
  emp_t *emp = NULL;
  emp_t *current;
  list_node_t *node = list_GetFirstNode(&dept->emps);
  list_node_GetData(node, &emp);
  for (
    node = list_node_GetNext(node);
    node != NULL;
    node = list_node_GetNext(node))
  {
    list_node_GetData(node, &current);
    if (CompareEmpSeniority(emp, current) == CMP_GREATER)
    {
      emp = current;
    }
  }
  return emp;
}

// Den Leiter der Abteilung ermitteln.
const emp_t *const dept_GetManager(const dept_t *const dept)
{
  emp_t *emp = NULL;
  for (
    list_node_t *node = list_GetFirstNode(&dept->emps);
    node != NULL;
    node = list_node_GetNext(node))
  {
    list_node_GetData(node, &emp);
    if (emp->job_type == JT_MANAGER)
    {
      return emp;
    }
  }
  return NULL;
}

void PutEmpToConsole(emp_t *emp)
{
  emp_PutToConsole(emp, "\n  ");
}

// Ausgabe einer kompletten Abteilung inkl. aller Angestellten.
void dept_PutToConsole(const dept_t *const dept)
{
  // Ausgabe der Angestellten dieser Abteilung.
  printf("Abteilung \"%s\" D#%010d (%s)", dept->name, dept->dept_no, dept->address);
  list_ForEach(&dept->emps, PutEmpToConsole);
  printf("\n");
}
