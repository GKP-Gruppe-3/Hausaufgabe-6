#ifndef __ABTEILUNG_H__
#define __ABTEILUNG_H__

#include "util.h"
#include "list.h"

typedef struct emp emp_t;

/***************************************
* Aufgabe 1.1 - Verbund f�r Abteilung:
***************************************/

typedef struct dept dept_t;
struct dept
{
  char *name;                 // Name (Zeichenkette)
  char *address;              // Adresse (Zeichenkette)
  int32_t dept_no;            // Abteilungsnummer (Ganzzahl)
  list_t emps;                // Liste der Angestellten der Abteilung (Verkette Liste)
  bool free_strings_in_dtor;  // M�ssen die Zeichenketten beim Zerst�ren freigegeben werden?
};

/**************************************/

void dept_Ctor(dept_t *dept, char *name, char *address, int32_t dept_no, bool free_strings_in_dtor);

dept_t *dept_Alloc(char *name, char *address, int32_t dept_no, bool free_strings_in_dtor);

void dept_Dtor(dept_t *dept);

void dept_Free(dept_t *dept);

/***************************************
* Aufgabe 1.3 - Funktionen f�r Abteilung (Header):
***************************************/

// Erstellen einer neuen Abteilung erm�glicht.
// Die Funktion soll interaktiv vom Nutzer Werte f�r Name,
// Adresse und Abteilungsnummer abfragen und eine entsprechende Abteilung erstellen.
dept_t dept_GetFromConsole();

// Hinzuf�gen eines neuen Angestellten zur Abteilung.
void dept_AddEmp(dept_t *dept, emp_t *emp);

// Entfernen eines Angestellten aus einer Abteilung.
void dept_RemEmp(dept_t *dept, emp_t *emp);

// Einen Angestellten von einer Abteilung A in eine Abteilung B verschieben.
void dept_TransEmp(dept_t *trg, dept_t *src, emp_t *emp);

// Berechnung des Gesamtgehaltes der Abteilung
// (sprich die Summe aller Geh�lter der Angestellten einer Abteilung).
double dept_GetSalSum(const dept_t *const dept);

// Den (kalendarisch) �ltesten Angestellten der Abteilung zur�ck gibt.
const emp_t *const dept_GetEmpByMaxAge(const dept_t *const dept);

// Den Angestellten mit der l�ngsten Betriebszugeh�rigkeit zur�ckgeben.
const emp_t *const dept_GetEmpByMaxSeniority(const dept_t *const dept);

// Den Leiter der Abteilung ermitteln.
const emp_t *const dept_GetManager(const dept_t *const dept);

// Ausgabe einer kompletten Abteilung inkl. aller Angestellten.
void dept_PutToConsole(const dept_t *const dept);

#endif
