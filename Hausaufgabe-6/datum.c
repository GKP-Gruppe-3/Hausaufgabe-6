#include "stdafx.h"

#include "datum.h"

const uint_fast16_t DAYS_TO_MONTH_365[] = { 0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
const uint_fast16_t DAYS_TO_MONTH_366[] = { 0, 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366 };

bool isLeapYear(const int_fast16_t year)
{
  if (year < 1 || year > 9999)
  {
    // Error: year out of range
    return false;
  }
  return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

date_t date_Now()
{
  time_t t = time(0);
  struct tm tm;
  localtime_s(&tm, &t);
  return (date_t) { tm.tm_year, tm.tm_mon + 1, tm.tm_mday };
}

/***************************************
* Aufgabe 1.5 - Erweiterung der Implementierung:
***************************************/

bool date_IsValid(const date_t *const date)
{
  if (
    (date->year < 1 || date->year > 9999) ||
    (date->month < 1 || date->month > 12) ||
    (date->day < 1))
  {
    return false;
  }
  switch (date->month)
  {
    case 2:
      return date->day <= (isLeapYear(date->year) ? 29 : 28 );
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      return date->day <= 30;
      break;
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return date->day <= 31;
      break;
  }
  return false;
}

cmp_t date_Compare(const date_t *const left, const date_t *const right)
{
  if (left->year < right->year)
  {
    return CMP_LESS;
  }
  else if (left->year > right->year)
  {
    return CMP_GREATER;
  }
  if (left->month < right->month)
  {
    return CMP_LESS;
  }
  else if (left->month > right->month)
  {
    return CMP_GREATER;
  }
  if (left->day < right->day)
  {
    return CMP_LESS;
  }
  else if (left->day > right->day)
  {
    return CMP_GREATER;
  }
  return CMP_EQUAL;
}

bool date_Equals(const date_t *const left, const date_t *const right)
{
  return
    (left->year == right->year) &&
    (left->month == right->month) &&
    (left->day == right->day);
}

uint_fast16_t date_DayOfYear(const date_t *const date)
{
  return date->day +
    (isLeapYear(date->year) ? DAYS_TO_MONTH_366[date->month] :
                              DAYS_TO_MONTH_365[date->month]);
}

int_fast32_t date_DiffYears(const date_t *const left, const date_t *const right)
{
  return left->year - right->year;
}

int_fast32_t date_DiffDays(const date_t *const left, const date_t *const right)
{
  cmp_t cmp = date_Compare(left, right);
  if (cmp == CMP_EQUAL)
  {
    return 0;
  }
  const date_t *const lesser_date = (cmp == CMP_LESS ? left : right);
  const date_t *const greater_date = (cmp == CMP_LESS ? right : left);
  int_fast32_t days;
  if (greater_date->year == lesser_date->year)
  {
    // Die abgelaufenen Jahrstage des gr��eren Datums
    // abz�glich derer des kleineren Datums.
    days = date_DayOfYear(greater_date) - date_DayOfYear(lesser_date);
  }
  else
  {
    // Die verbleibenden Jahrstage des kleineren Datums
    // zuz�glich der abgelaufenen Jahrstage des gr��eren Datums.
    days =
      (isLeapYear(lesser_date->year) ? 366 : 365) - date_DayOfYear(lesser_date) +
      date_DayOfYear(greater_date);
    // Die Tage der vollen zwischenliegenden Jahre aufaddieren.
    for (uint_fast16_t y = lesser_date->year + 1; y < greater_date->year; y++)
    {
      days += (isLeapYear(y) ? 366 : 365);
    }
  }
  return days * -cmp;
}

/***************************************
* Aufgabe 1.3 - Implementierung:
***************************************/

#define INPUT_SIZE 64

date_t date_GetFromConsole(const char *const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  date_t date = { 0, 0, 0 };
  char str[INPUT_SIZE];
  string_GetFromConsole(str, INPUT_SIZE, NULL);
  _strlwr_s(str, INPUT_SIZE);
  int i = 0;
  char *found, *next;
  const char delimiter[] = ". ";
  found = strtok_s(str, delimiter, &next);
  while (found != NULL)
  {
    if (strlen(found) > 0)
    {
      switch (i)
      {
        case 0:
          date.day = atoi(found);
          break;
        case 1:
          if (strcmp(found, "jan") == 0 || strcmp(found, "januar") == 0)
          {
            date.month = 1;
          }
          else if (strcmp(found, "feb") == 0 || strcmp(found, "februar") == 0)
          {
            date.month = 2;
          }
          else if (strcmp(found, "mar") == 0 || strcmp(found, "maerz") == 0)
          {
            date.month = 3;
          }
          else if (strcmp(found, "apr") == 0 || strcmp(found, "april") == 0)
          {
            date.month = 4;
          }
          else if (strcmp(found, "mai") == 0)
          {
            date.month = 5;
          }
          else if (strcmp(found, "jun") == 0 || strcmp(found, "juni") == 0)
          {
            date.month = 6;
          }
          else if (strcmp(found, "jul") == 0 || strcmp(found, "juli") == 0)
          {
            date.month = 7;
          }
          else if (strcmp(found, "aug") == 0 || strcmp(found, "august") == 0)
          {
            date.month = 8;
          }
          else if (strcmp(found, "sep") == 0 || strcmp(found, "september") == 0)
          {
            date.month = 9;
          }
          else if (strcmp(found, "okt") == 0 || strcmp(found, "oktober") == 0)
          {
            date.month = 10;
          }
          else if (strcmp(found, "nov") == 0 || strcmp(found, "november") == 0)
          {
            date.month = 11;
          }
          else if (strcmp(found, "dez") == 0 || strcmp(found, "dezember") == 0)
          {
            date.month = 12;
          }
          else
          {
            date.month = atoi(found);
          }
          break;
        case 2:
          date.year = atoi(found);
          break;
        default:
          // Error
          return (date_t){ 0, 0, 0 };
          break;
      }
      i++;
    }
    found = strtok_s(NULL, delimiter, &next);
  }
  return date;
}

void date_PutToConsole(const date_t *const date, bool long_representation)
{
  printf("%02d.", date->day);
  if (long_representation)
  {
    switch (date->month)
    {
      case 1:
        printf(" Januar ");
        break;
      case 2:
        printf(" Februar ");
        break;
      case 3:
        printf(" Maerz ");
        break;
      case 4:
        printf(" April ");
        break;
      case 5:
        printf(" Mai ");
        break;
      case 6:
        printf(" Juni ");
        break;
      case 7:
        printf(" Juli ");
        break;
      case 8:
        printf(" August ");
        break;
      case 9:
        printf(" September ");
        break;
      case 10:
        printf(" Oktober ");
        break;
      case 11:
        printf(" November ");
        break;
      case 12:
        printf(" Dezember ");
        break;
      default:
        printf("%02d.", date->month);
        break;
    }
  }
  else
  {
    printf("%02d.", date->month);
  }
  printf("%04d", date->year);
}

/***************************************
* Aufgabe 1.7 - Testprogramm (Implementation):
***************************************/

void date_Test()
{
  /*date_t d1 = date_GetFromConsole();
  date_t d2 = date_GetFromConsole();

  bool is_valid_1 = date_IsValid(&d1);
  bool is_valid_2 = date_IsValid(&d2);
  uint_fast16_t day_of_year_1 = date_DayOfYear(&d1);
  uint_fast16_t day_of_year_2 = date_DayOfYear(&d2);
  bool equals = date_Equals(&d1, &d2);
  uint_fast32_t diff_days = date_DiffDays(&d1, &d2);

  printf("\nDatum 1 (Wert) = ");
  date_PutToConsole(&d1, false);
  printf("\nIst Datum 1 gueltig? = %s\n", (is_valid_1 ? "Ja" : "Nein"));
  printf("Datum 1 - Tag des Jahres = %ld\n", day_of_year_1);

  printf("\nDatum 2 (Wert) = ");
  date_PutToConsole(&d2, true);
  printf("\nIst Datum 2 gueltig? = %s\n", (is_valid_2 ? "Ja" : "Nein"));
  printf("Datum 2 - Tag des Jahres = %ld\n", day_of_year_2);

  printf("\nSind die beiden Datumswerte gleich? = %s\n", (equals ? "Ja" : "Nein"));
  printf("Die Differenz der beiden Datumswerte in Tagen = %ld\n\n", diff_days);

  Pause();*/
}
